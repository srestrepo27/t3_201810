package testEstructuras;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.ListaCola;
import model.vo.Taxi;

public class ColaTest
{
		private ListaCola cola;
		
		@Before
		public void setupEscenario1()
		{
			cola= new ListaCola<>();
		}
		@Before
		public void setupEscenario2()
		{
			cola= new ListaCola<>();
			Taxi elemento= new Taxi("abc","Compa�ia");
			cola.enqueue(elemento);
		}
		@Test
		public void enqueueTest()
		{
			Taxi elemento= new Taxi("abc","Compa�ia");
			setupEscenario1();
			try
			{
				cola.enqueue(elemento);
				assertNotNull("Se deberia agregar el taxi",cola.dequeue());

			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
		@Test
		public void deleteTest()
		{
			setupEscenario2();
			try
			{
				cola.dequeue();
				assertTrue("El tama�o deberia ser 0",cola.isEmpty());
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		
	}

